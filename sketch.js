
let synthState;
let voice1;
let voice2;  



function setup() {
    createCanvas(windowWidth * 0.98, windowHeight * 0.98);
    synthState = new SynthState(4);

}

function draw() {
    background(0);

}


// Sets synthstate for arrow presses
function keyPressed() {
    switch (keyCode) {
        case LEFT_ARROW:
            synthState.semiTone--;
            break;
        case RIGHT_ARROW:
            synthState.semiTone++;
            break;
        case DOWN_ARROW:
            synthState.octave--;
            break;
        case UP_ARROW:
            synthState.octave++;
            break;
    }

}


// Whenever the keyboard is pressed.
function keyTyped() {

    let freq;
    let note = keyboardToNote(key);
    print(note);
    
    if (note !== null) {

        freq = calculateFreq(note, synthState);
        oscType = keyboardToOscType(key);

        synthState.voices.forEach((voice) => {
            voice.notePressed(freq);
        });
    }
}




