// Defines a Voice class
class SoloVoice {
    constructor(type = 1) {
        
        this.osc = new p5.Oscillator();

        this.type = keyboardToOscType(type.toString());
        this.osc.setType(this.type); 
        
        
        this.env = new p5.Envelope();

        this.eq = new p5.EQ(3);
        this.eq.process(this.osc);
        this.eq.bands[0].gain(40);
        this.eq.bands[1].gain(-100);
        this.eq.bands[2].gain(100);



        this.delay = new p5.Delay();
        this.delay.process(this.osc, .12, .7, 2300);

        this.reverb = new p5.Reverb();
        this.reverb.process(this.osc, 2, 4);
        


        
        this.attack = 0.1;
        this.decay = 0.2;
        this.sustain = 0.2;
        this.release = 0.5;

        this.env.setADSR(this.attack, this.decay, this.sustain, this.release);
        this.env.setRange(0.8, 0); 


              
    }


    notePressed(freq) {
        this.osc.setType(oscType);
        this.osc.amp(this.env);
        this.osc.freq(freq);
        this.osc.start();
        this.env.play();
    }

}



class SynthState {
    constructor(numVoices) {
        this.octave = 0;
        this.semiTone = 0;
        this.numvoices = numVoices;
        this.voices = [];

        let oType = 1;

        for (let i = 0; i < numVoices; i++) {

            this.voices.push(new SoloVoice(oType));

            (oType < 4) ? oType++ : oType = 1;
        }  
    }
}