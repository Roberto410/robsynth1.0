
// Returns the frequencies that a computer keyboard refers too
function keyboardToNote(key) {

    switch (key) {
        default:
            return null
        case 'z':
            return -9
        case 's':
            return -8
        case 'x':
            return -7
        case 'd':
            return -6
        case 'c':
            return -5
        case 'v':
            return -4
        case 'g':
            return -3
        case 'b':
            return -2
        case 'h':
            return -1
        case 'n':
            return 0
        case 'j':
            return 1
        case 'm':
            return 2
        case ',':
            return 3
        case 'l':
            return 4
        case '.':
            return 5
        case ';':
            return 6
        case '/':
            return 7
    }

}

// returns the osc type depending on the key pressed
function keyboardToOscType(key) {

    if (key == '1'){	// set the note type to sine
        return 'sine'
    }	
    if (key == '2'){	// set the note type to square
        return 'square'
    }	
    if (key == '3'){	// set the note type to triangle
        return 'triangle'
    }	
    if (key == '4'){	// set the note type to sawtooth
        return 'sawtooth'
    }

}


// returns the correct frequency for the note depending on the overal octave and semiton settings
function calculateFreq(semitonesFromA4, synth) {
    let freq

    semitonesFromA4 += (synth.octave * 12) + (synth.semiTone);

    freq = 440 * Math.pow((Math.pow(2, 1/12)), semitonesFromA4);

    return freq
}